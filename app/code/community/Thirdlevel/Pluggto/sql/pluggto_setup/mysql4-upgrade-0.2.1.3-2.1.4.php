<?php

try{

    $installer = new Mage_Sales_Model_Mysql4_Setup('core_setup');
    $installer->startSetup();
    $installer->run("CREATE INDEX status ON thirdlevel_pluggto_line (status)");
    $installer->endSetup();

} catch (exception $e){
        Mage::log(print_r($e,true));
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pluggto')->__('A atualização do Pluggto falhou, verifique o log de erro.'));

}