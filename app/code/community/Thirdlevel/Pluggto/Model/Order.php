<?php

class Thirdlevel_Pluggto_Model_Order extends Mage_Core_Model_Abstract
{

    public $weight;
    public $totalqtd;
    public $configs;

    protected function _construct()
    {

        $this->_init("pluggto/order");
    }

    public function getConfig()
    {

        if (empty($this->configs)) {
            $this->configs = Mage::helper('pluggto')->config();
        }

        return $this->configs;
    }

    public function convertToStateLogName($shortName){

        if(strlen($shortName) > 2){
            return $shortName;
        }

        $shortName = strtoupper($shortName);

        $estadosBrasileiros = array(
            'AC'=>'Acre',
            'AL'=>'Alagoas',
            'AP'=>'Amapá',
            'AM'=>'Amazonas',
            'BA'=>'Bahia',
            'CE'=>'Ceará',
            'DF'=>'Distrito Federal',
            'ES'=>'Espírito Santo',
            'GO'=>'Goiás',
            'MA'=>'Maranhão',
            'MT'=>'Mato Grosso',
            'MS'=>'Mato Grosso do Sul',
            'MG'=>'Minas Gerais',
            'PA'=>'Pará',
            'PB'=>'Paraíba',
            'PR'=>'Paraná',
            'PE'=>'Pernambuco',
            'PI'=>'Piauí',
            'RJ'=>'Rio de Janeiro',
            'RN'=>'Rio Grande do Norte',
            'RS'=>'Rio Grande do Sul',
            'RO'=>'Rondônia',
            'RR'=>'Roraima',
            'SC'=>'Santa Catarina',
            'SP'=>'São Paulo',
            'SE'=>'Sergipe',
            'TO'=>'Tocantins'
        );

        if(isset($estadosBrasileiros[$shortName])){
            return $estadosBrasileiros[$shortName];
        } else {
            return $shortName;
        }


    }

    public function convertToStateShortName($name){

        if(strlen($name) == 2){
            return $name;
        }

        $estadosBrasileiros = array(
            'AC'=>'acre',
            'AL'=>'alagoas',
            'AP'=>'amapa',
            'AM'=>'amazonas',
            'BA'=>'bahia',
            'CE'=>'ceara',
            'DF'=>'distrito federal',
            'ES'=>'espírito santo',
            'GO'=>'goias',
            'MA'=>'maranhao',
            'MT'=>'mato grosso',
            'MS'=>'mato grosso do sul',
            'MG'=>'minas gerais',
            'PA'=>'para',
            'PB'=>'paraiba',
            'PR'=>'parana',
            'PE'=>'pernambuco',
            'PI'=>'piaui',
            'RJ'=>'rio de janeiro',
            'RN'=>'rio grande do norte',
            'RS'=>'rio grande do sul',
            'RO'=>'rondonia',
            'RR'=>'roraima',
            'SC'=>'santa catarina',
            'SP'=>'sao paulo',
            'SE'=>'sergipe',
            'TO'=>'tocantins'
        );
        // retira acentos para fazer a busca
        $newname = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$name);
        // deixa tudo em minuscula

        $newname = strtolower($newname);

        // faz busca

        if(array_search($newname,$estadosBrasileiros)){
            return array_search($newname,$estadosBrasileiros);
        } else {
            return $name;
        }



    }


    // create item
    private function importItem($unitem,$product=null)
    {


        $items = new Mage_Sales_Model_Order_Item();

        if (isset($unitem['variation']['sku'])) {
            $sku = $unitem['variation']['sku'];
        } elseif (isset($unitem['sku'])) {
            $sku = $unitem['sku'];
        }


        if (isset($sku) && empty($product)) {
            $product = Mage::getModel('pluggto/product')->findProduct($sku);
        }


        if (!$product && isset($unitem['sku'])) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $unitem['sku']);
        }


        if (isset($product) && is_object($product) && $product->getEntityId() != null) {
            $items->setProductId($product->getEntityId());
            $items->setProductType($product->getTypeId());
            $items->setProductWeight($product->getWeight());
            $this->weight += $product->getWeight();
        }


        $items->setBaseWeeeTaxAppliedAmount(0);
        $items->setBaseWeeeTaxAppliedRowAmnt(0);
        $items->setWeeeTaxAppliedAmount(0);
        $items->setWeeeTaxAppliedRowAmount(0);
        $items->setWeeeTaxApplied(serialize(array()));
        $items->setWeeeTaxDisposition(0);
        $items->setWeeeTaxRowDisposition(0);
        $items->setBaseWeeeTaxDisposition(0);
        $items->setBaseWeeeTaxRowDisposition(0);


        if (isset($unitem['name'])) $items->setName($unitem['name']);
        if (isset($unitem['price'])) $items->setBasePrice($unitem['price']);
        if (isset($unitem['sku']) && isset($unitem['quantity'])) $items->setRowTotal($unitem['price'] * $unitem['quantity']);
        if (isset($unitem['price'])) $items->setOriginalPrice($unitem['price']);
        if (isset($unitem['price'])) $items->setPrice($unitem['price']);
        if (isset($unitem['quantity'])) $items->setQtyOrdered($unitem['quantity']);
        if (isset($unitem['quantity'])) $this->totalqtd += $unitem['quantity'];
        if (isset($unitem['total'])) $items->setRowTotal($unitem['total']);
        if (isset($unitem['total'])) $items->setSubtotal($unitem['total']);

        if (isset($unitem['variation']['sku'])) {


            $subproduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $unitem['variation']['sku']);

            if($subproduct){
                $items->setProductId($subproduct->getEntityId());
                $items->setProductType($subproduct->getTypeId());
                $items->setProductWeight($subproduct->getWeight());
            }

            $items->setSku($unitem['variation']['sku']);

        } elseif (isset($unitem['sku'])) {
            $items->setSku($unitem['sku']);
        }

        $attributes = array();

        if (isset($unitem['variation']['attributes']) && is_array($unitem['variation']['attributes'])) {

            foreach ($unitem['variation']['attributes'] as $att) {
                if (isset($att['label']) && isset($att['value']['label'])) $attributes[] = $att['label'] . ':' . $att['value']['label'] . ' ';
            }

            $items->setAdditionalData(implode(',', $attributes));

        }

        return $items;
    }


    // create at store
    public function create($data)
    {


        if (!Mage::getStoreConfig('pluggto/orders/allowcreate')) {
            return;
        }

        try {
            $customer = Mage::getModel('pluggto/customer')->getCustomer($data);
        } catch (exception $e) {
            $customer = false;
        }


        $order = new Mage_Sales_Model_Order();
        $col = $order->getCollection();

        $order = $col->addFieldToFilter('plugg_id', $data['id'])->getFirstItem();

        $new = false;

        if ($order->getEntityId() == null) {
            $new = true;
            $order = new Mage_Sales_Model_Order();
        }

        if ($order->getCanalId() == null) {
            if (isset($data['external'][$data['created_by']])) {
                $order->setCanalId($data['external'][$data['created_by']]);
                $order->setExtOrderId($data['external'][$data['created_by']]);
            }
        }

        if ($order->getCanal() == null) {
            if (isset($data['created_by'])) {
                $api = Mage::getSingleton('pluggto/api')->load(1);
                $canalReturn = $api->get('clientInfo/' . $data['created_by'], null, null, true);

                if (isset($canalReturn['Body']['name'])) {
                    $order->setCanal($canalReturn['Body']['name']);
                    $order->setExtOrderId($canalReturn['Body']['name'] . '-' . $data['external'][$data['created_by']]);
                }

            }
        }

        if (!empty($data['payer_email'])) {
            $order->setCustomerEmail($data['payer_email']);
        } elseif (!empty($data['receiver_email'])) {
            $order->setCustomerEmail($data['receiver_email']);
        } else {
            $order->setCustomerEmail('customer@email.com');
        }

        $order->setCustomerFirstname($data['payer_name']);
        $order->setCustomerLastname($data['payer_lastname']);
        $order->setPluggId($data['id']);


        if (isset($customer) && is_array($customer) && isset($customer['id'])) {
            $order->setCustomerId($customer['id']);
        } else {
            $order->setCustomerIsGuest(1);
        }


        if (isset($data['payer_cpf']) && !empty($data['payer_cpf'])) {
            $order->setCustomerTaxvat($data['payer_cpf']);
        }

        if (isset($data['payer_cnpj']) && !empty($data['payer_cnpj'])) {
            $order->setCustomerTaxvat($data['payer_cnpj']);
        }

        if (isset($data['payer_cpf']) && !empty($data['payer_cpf'])) $document = $data['payer_cpf'];
        if (isset($data['payer_cnpj']) && !empty($data['payer_cnpj'])) $document = $data['payer_cnpj'];


        $customFieldToStoreCFPorCNPJ = Mage::getStoreConfig('pluggto/configs/custom_document_field');


        if (isset($document) && $customFieldToStoreCFPorCNPJ != '' && $customFieldToStoreCFPorCNPJ != null) {
            $order->addData(array(trim($customFieldToStoreCFPorCNPJ) => $document));
        }

        if(isset($data['price_code']) && !empty($data['price_code'])){

            $store = $this->getStoreByCode($data['price_code']);

            if(empty($store)){
                $store = Mage::getStoreConfig('pluggto/configs/default_store');
            }

        } else {
            $store = Mage::getStoreConfig('pluggto/configs/default_store');
        }




        if (!empty($store)) {
            $currencies_array = Mage::app()->getStore($store)->getDefaultCurrency();
        } else {
            $store = Mage::app()->getStore();
            $currencies_array = Mage::app()->getStore()->getDefaultCurrency();
        }

        $currencycode = $currencies_array->getCurrencyCode();

        if (empty($data['subtotal']) || $data['subtotal'] == 0) {
            $data['subtotal'] = $data['total'] - $data['shipping'];
        }

        // total amount informatiom
        $order->setTotalDue($data['total']);
        $order->setSubtotal($data['subtotal']);
        $order->setGrandTotal($data['total']);
        $order->setTotalDue($data['total']);
        $order->setBaseTaxAmount(0.00);
        $order->setBaseGrandTotal($data['total']);
        $order->setStoreCurrencyCode($currencycode);
        $order->setShippingAmount($data['shipping']);
        $order->setBaseShippingAmount($data['shipping']);

        $order->setBaseSubtotalInclTax($data['subtotal']);
        $order->setSubtotalInclTax($data['subtotal']);
        $order->setShippingDiscount(0);
        $order->setStoreId($store);
        $order->setCurrenyCode($currencycode);
        $order->setOrderCurrencyCode($currencycode);
        $order->setGlobalCurrencyCode($currencycode);
        $order->setBaseCurrencyCode($currencycode);
        $order->setBaseSubtotal($data['subtotal']);
        $order->setBaseToOrderRate(1);
        $order->setBaseToGlobalRate(1);
        $order->setBaseTaxAmount(0);


        // billing information

        if ($order->getIncrementId()) {
            $billing = Mage::getModel('sales/order_address')->load($order->getBillingAddress()->getId());
        } else {
            $billing = new Mage_Sales_Model_Order_Address;
        }

        $billing->setFirstname($data['payer_name']);
        $billing->setLastname($data['payer_lastname']);

        $PayerAddressLine = array();

        // receiver address line
        if (!empty($data['payer_address'])) {
            $PayerAddressLine[] = $data['payer_address'];
        } else {
            $PayerAddressLine[] = '';
        }

        if (!empty($data['payer_address_number'])) {
            $PayerAddressLine[] = $data['payer_address_number'];
        } else {
            $PayerAddressLine[] = '';
        }

        if (!empty($data['payer_address_complement'])) {

            if (!empty($data['payer_additional_info'])) {
                $PayerAddressLine[] = $data['payer_address_complement'] . '-' . $data['payer_additional_info'];
            } else {
                $PayerAddressLine[] = $data['payer_address_complement'];
            }

        } else {

            if (!empty($data['payer_additional_info'])) {
                $PayerAddressLine[] = $data['payer_additional_info'];
            } else {
                $PayerAddressLine[] = '';
            }
        }

        if (!empty($data['payer_neighborhood'])) {
            $PayerAddressLine[] = $data['payer_neighborhood'];
        } else {
            $PayerAddressLine[] = '';
        }


        if (!empty($PayerAddressLine)) {
            $billing->setStreet($PayerAddressLine);
        }

        if (isset($data['payer_zipcode'])) {
            $billing->setPostcode($data['payer_zipcode']);
        }
        if (isset($data['payer_city'])) {
            $billing->setCity($data['payer_city']);
        }


        $stateFormat = Mage::getStoreConfig('pluggto/configs/state_format');

        if (isset($data['payer_state'])) {

            if(!empty($stateFormat)){

                if($stateFormat == 'short'){
                    $BillingState = $this->convertToStateShortName($data['payer_state']);
                } else if($stateFormat == 'long'){
                    $BillingState = $this->convertToStateLogName($data['payer_state']);
                } else {
                    $BillingState = $data['payer_state'];
                }

            } else {
                $BillingState = $data['payer_state'];
            }
        }

        if (isset($BillingState)) {
            $billing->setRegion($BillingState);
        }

        if (isset($data['payer_country'])) {
            $billing->setCountry($data['payer_country']);
        }

        if (isset($data['payer_phone']) && isset($data['payer_phone_area'])) {
            $billing->setTelephone($data['payer_phone_area'] . $data['payer_phone']);
        }
        if (isset($data['payer_email'])) {
            $billing->setEmail($data['payer_email']);
        }
        if (isset($data['payer_cpf'])) {
            $billing->setVatId($data['payer_cpf']);
        }

        if (isset($data['payer_cnpj'])) {
            $billing->setVatId($data['payer_cnpj']);
        }


        $billing->setCountryId($data['payer_country']);

        $regionModel = Mage::getModel('directory/region')->loadByCode($data['payer_state'], $data['payer_country']);
        $regionId = $regionModel->getId();


        $billing->setRegionId($regionId);

        if (!$order->getIncrementId()) {
            $order->setBillingAddress($billing);
        }

        // shipping information
        if ($order->getIncrementId()) {
            $shipping = Mage::getModel('sales/order_address')->load($order->getShippingAddress()->getId());
        } else {
            $shipping = new Mage_Sales_Model_Order_Address;
        }


        $shipping->setFirstname($data['receiver_name']);
        $shipping->setLastname($data['receiver_lastname']);

        $ReceiverAddressLine = array();
        // receiver address line

        if (!empty($data['receiver_address'])) {
            $ReceiverAddressLine[] = $data['receiver_address'];
        } else {
            $ReceiverAddressLine[] = '';
        }

        if ($data['receiver_address_number'] != null && $data['receiver_address_number'] != '') {
            $ReceiverAddressLine[] = $data['receiver_address_number'];
        } else {
            $ReceiverAddressLine[] = '';
        }

        if (!empty($data['receiver_address_complement'])) {

            if (!empty($data['receiver_additional_info'])) {

                if (!empty($data['receiver_address_reference'])) {
                    $ReceiverAddressLine[] = $data['receiver_address_complement'] . '-' . $data['receiver_additional_info'] . '-' . $data['receiver_address_reference'];
                } else {
                    $ReceiverAddressLine[] = $data['receiver_address_complement'] . '-' . $data['receiver_additional_info'];
                }

            } else {

                if (!empty($data['receiver_address_reference'])) {
                    $ReceiverAddressLine[] = $data['receiver_address_complement'] . '-' . $data['receiver_address_reference'];
                } else {
                    $ReceiverAddressLine[] = $data['receiver_address_complement'];
                }

            }

        } else {

            if (!empty($data['receiver_additional_info'])) {

                if (!empty($data['receiver_address_reference'])) {
                    $ReceiverAddressLine[] = $data['receiver_additional_info'] . '-' . $data['receiver_address_reference'];
                } else {
                    $ReceiverAddressLine[] = $data['receiver_additional_info'];
                }

            } else {

                if (!empty($data['receiver_address_reference'])) {
                    $ReceiverAddressLine[] = $data['receiver_address_reference'];
                } else {
                    $ReceiverAddressLine[] = '';
                }
            }
        }

        if (!empty($data['receiver_neighborhood'])) {
            $ReceiverAddressLine[] = $data['receiver_neighborhood'];
        } else {
            $ReceiverAddressLine[] = '';
        }

        if (!empty($ReceiverAddressLine)) {
            $shipping->setStreet($ReceiverAddressLine);
        }

        if (isset($data['receiver_zipcode'])) {
            $shipping->setPostcode($data['receiver_zipcode']);
        }

        if (isset($data['receiver_city'])) {
            $shipping->setCity($data['receiver_city']);
        }

        if (isset($data['receiver_state'])) {

            if(!empty($stateFormat)){

                if($stateFormat == 'short'){
                    $ReceiverState = $this->convertToStateShortName($data['receiver_state']);
                } else if($stateFormat == 'long'){
                    $ReceiverState = $this->convertToStateLogName($data['receiver_state']);
                } else {
                    $ReceiverState = $data['receiver_state'];
                }

            } else {
                $ReceiverState = $data['receiver_state'];
            }
        }


        if (isset($ReceiverState)) {
            $shipping->setRegion($ReceiverState);
        }

        if (isset($data['receiver_phone'])) {
            $shipping->setTelephone($data['receiver_phone']);
        }
        if (isset($data['receiver_email'])) {
            $shipping->setEmail($data['receiver_email']);
        }

        if (is_null($data['receiver_country'])) {
            $data['receiver_country'] = 'BR';
        }

        $shipping->setCountryId($data['receiver_country']);


        $regionModel = Mage::getModel('directory/region')->loadByCode($data['receiver_state'], $data['receiver_country']);
        $regionId = $regionModel->getId();

        $shipping->setRegionId($regionId);

        if (!$order->getIncrementId()) {
            $order->setShippingAddress($shipping);
        }


        $items = $order->getAllVisibleItems();


        if (!$order->getIncrementId() || count($items) == 0) {

            if (count($data['items']) == 0) {

                $items = new Mage_Sales_Model_Order_Item();
                $items->setBasePrice($data['subtotal']);
                $items->setProductId(0);
                $items->setRowTotal($data['subtotal']);
                $items->setOriginalPrice($data['subtotal']);
                $items->setPrice($data['subtotal']);
                $items->setQtyOrdered();
                $items->setProductWeight(0.1);
                $items->setSku('pluggto');
                $items->setName(Mage::helper('pluggto')->__('Produto do PluggTo'));
                $order->addItem($items);

            } else {
                foreach ($data['items'] as $unitem) {

                    if (isset($unitem['sku'])) {


                        $product = Mage::getModel('pluggto/product')->findProduct($unitem['sku']);

                        if($product) {

                            // need to upadte stock from here
                            if ($product->getTypeId() == 'grouped') {

                                $associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);

                                foreach ($associatedProducts as $option) {
                                    if ($option->getQty() > 0) {
                                        $quantityPerItem = $option->getQty();
                                    } else {
                                        $quantityPerItem = 1;
                                    }

                                    $stock = $option->getStockItem();

                                    $atualQuantidadeEmEstoque = $stock->getQty();


                                    $totaldeQuantidadeComprada = $quantityPerItem * $unitem['quantity'];

                                    $finalQuantidade = $atualQuantidadeEmEstoque - $totaldeQuantidadeComprada;


                                    $stock->setQty($finalQuantidade);
                                    $stock->save();

                                    $gproduct = Mage::getModel('catalog/product')->load($stock->getProductId());

                                    Mage::dispatchEvent('catalog_product_save_after', array('product' => $gproduct));

                                    $unitem['sku'] = $gproduct->getSku();
                                    $unitem['name'] = $product->getName() . '-' . $gproduct->getName();
                                    $unitem['price'] = $gproduct->getPrice();
                                    $unitem['total'] = $gproduct->getPrice() * $totaldeQuantidadeComprada;
                                    $unitem['quantity'] = $totaldeQuantidadeComprada;

                                    $items = $this->importItem($unitem, $gproduct);
                                    $order->addItem($items, $product);
                                }


                                $order->addStatusHistoryComment('Order has grouped products');

                                // need to upadte stock from here
                            } else if ($product->getTypeId() == 'bundle') {

                                $selectionCollection = $product->getTypeInstance(true)->getSelectionsCollection($product->getTypeInstance(true)->getOptionsIds($product),
                                    $product);

                                foreach ($selectionCollection as $option) {


                                    if ($option->getSelectionQty() > 0) {
                                        $quantidadePorPacote = $option->getSelectionQty();
                                    } else {
                                        $quantidadePorPacote = 1;
                                    }

                                    $stock = $option->getStockItem();
                                    $atualQuantidadeEmEstoque = $stock->getQty();


                                    $totaldeQuantidadeComprada = $quantidadePorPacote * $unitem['quantity'];

                                    $finalQuantidade = $atualQuantidadeEmEstoque - $totaldeQuantidadeComprada;


                                    $stock->setQty($finalQuantidade);
                                    $stock->save();

                                    $gproduct = Mage::getModel('catalog/product')->load($stock->getProductId());

                                    Mage::dispatchEvent('catalog_product_save_after', array('product' => $gproduct));

                                    $unitem['sku'] = $gproduct->getSku();
                                    $unitem['name'] = $product->getName() . '-' . $gproduct->getName();
                                    $unitem['price'] = $gproduct->getPrice();
                                    $unitem['total'] = $gproduct->getPrice() * $totaldeQuantidadeComprada;
                                    $unitem['quantity'] = $totaldeQuantidadeComprada;

                                    $items = $this->importItem($unitem, $gproduct);
                                    $order->addItem($items, $product);
                                }


                                $order->addStatusHistoryComment('Order has bundle products');

                            } else {
                                $items = $this->importItem($unitem, $product);


                                $order->addItem($items, $product);
                            }
                        } else {

                            $items = $this->importItem($unitem, $product);

                            $order->addItem($items, $product);
                        }

                    }
                }
            }

            $order->setTotalQtyOrdered($this->totalqtd);
            $order->setWeight($this->weight);

        }


        if (!empty($data['delivery_type'])) {


            switch ($data['delivery_type']) {

                case 'standard':
                    $method = Mage::getStoreConfig('pluggto/shipping/standard');
                    break;
                case 'express':
                    $method = Mage::getStoreConfig('pluggto/shipping/express');
                    break;
                case 'onehour':
                    $method = Mage::getStoreConfig('pluggto/shipping/onehour');
                    break;
                case 'pickup':
                    $method = Mage::getStoreConfig('pluggto/shipping/pickup');
                    break;
                case 'economy':
                    $method = Mage::getStoreConfig('pluggto/shipping/economy');
                    break;
                case 'guaranteed':
                    $method = Mage::getStoreConfig('pluggto/shipping/guaranteed');
                    break;
                case 'scheduled':
                    $method = Mage::getStoreConfig('pluggto/shipping/scheduled');
                    break;
                default:
                    $method = Mage::getStoreConfig('pluggto/shipping/standard');

            }

        }

        if (!isset($method) || empty($method) ) {
            $method = Mage::getStoreConfig('pluggto/shipping/standard');

            if (empty($method)) {
                $method = Mage::getModel('pluggto/source_ShippingMethods')->toOptionArray();

                if (isset($method[1]) && isset($method[1]['value'])) {
                    $method = $method[1]['value'];
                }
            }
        }


        if(is_string($method)){
            $description = Mage::getStoreConfig("carriers/".$method."/title");
        } else {
            $method = 'PluggTo';
        }


        if(empty($description)){
            $description = $data['delivery_type'];
        } elseif(empty($description) && !empty($data['delivery_type'])) {
            $description = $method . '('.$data['delivery_type'].')';
        }




        if(empty($description)){
                $order->setShippingDescription('Método ('.$method.')');
        } else {
            $order->setShippingDescription($description);
        }


        if (isset($data['shipments'][0]['id'])) {
            $order->setShipmentId($data['shipments'][0]['id']);
        }


        // payment info
        $payment = new Mage_Sales_Model_Order_Payment();

        $storemmethod = Mage::getStoreConfig('pluggto/configs/paymentdefault');

        if (!empty($storemmethod)) {
            $payment->setMethod($storemmethod);
        } else {
            $payment->setMethod('pluggto');
        }

        if (isset($data['payment_method'])) {

            if ($data['payment_method']) {
                // caso pagamento tenha sido realizado por MercadoPago

                $payment->setAdditionalData($data['payment_method']);
            }
        }


        Mage::getSingleton('core/session')->setPluggToNotSave(1);
        $payment->setOrder($order);
        Mage::getSingleton('core/session')->setPluggToNotSave();


        if ($new) {
            $order->addPayment($payment->place());
            $order->setStatus(Mage::getStoreConfig('pluggto/orderstatus/pending'));
        }


        Mage::getSingleton('core/session')->setPluggToNotSave(1);
        $order->save();
        Mage::getSingleton('core/session')->setPluggToNotSave();

        if(!isset($data['external']) ||
            !is_array($data['external']) ||
            !in_array($order->getIncrementId(),$data['external'])
        ){
            // export order id
            Mage::getSingleton('pluggto/export')->exportOrderExternalId($order);
        }




        $shipping->save();
        $billing->save();
        $invoice = false;
        switch ($data['status']) {

            case 'approved':
            case 'paid':

                $status = Mage::getStoreConfig('pluggto/orderstatus/approved');
                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $invoice = true;
                if (Mage::getStoreConfig('pluggto/orders/invoice') == 1) {
                    $notifyCustomerOrderUpdate = false;
                    Mage::getSingleton('core/session')->setPluggToNotSave(1);
                    // Cria invoice (fatura) para o pedido se já não houver alguma criada.


                    try {
                        if (!$order->hasInvoices()) {

                            foreach ($order->getAllItems() as $item) {
                                $Allitems[$item->getId()] = $item->getQtyOrdered();
                            }

                            $invoice = $order->prepareInvoice();
                            $invoice->register()->pay();

                            Mage::getModel('core/resource_transaction')->addObject($invoice)->addObject($invoice->getOrder())->save();
                        }
                    } catch (exception $e) {
                    }
                    Mage::getSingleton('core/session')->setPluggToNotSave();
                }


                break;
            case 'partial_payment':
                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $status = Mage::getStoreConfig('pluggto/orderstatus/partial_payment');
                break;
            case 'refunded':
                $state = Mage_Sales_Model_Order::STATE_CANCELED;
                $status = Mage::getStoreConfig('pluggto/orderstatus/canceled');
                break;
            case 'pending':
                $status = Mage::getStoreConfig('pluggto/orderstatus/pending');
                $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
                break;
            case 'invoiced':
                $status = Mage::getStoreConfig('pluggto/orderstatus/invoiced');
                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $invoice = true;
                break;
            case 'under_review':
                $status = Mage::getStoreConfig('pluggto/orderstatus/under_review');
                $state = Mage_Sales_Model_Order::STATE_HOLDED;
                break;
            case 'canceled':
                $status = Mage::getStoreConfig('pluggto/orderstatus/canceled');
                $state = Mage_Sales_Model_Order::STATE_CANCELED;
                break;
            case 'delivered':
                $status = Mage::getStoreConfig('pluggto/orderstatus/delivered');
                $invoice = true;
                break;
            case 'shipped':
            case 'shipping_informed':
            case 'shipping_error':
                $status = Mage::getStoreConfig('pluggto/orderstatus/shipped');
                $invoice = true;
                break;
            default:
                $status = '';
                $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
                break;
        }

        if ($invoice) {
            if (Mage::getStoreConfig('pluggto/orders/invoice') == 1) {
                $notifyCustomerOrderUpdate = false;
                Mage::getSingleton('core/session')->setPluggToNotSave(1);
                // Cria invoice (fatura) para o pedido se já não houver alguma criada.


                try {
                    if (!$order->hasInvoices()) {

                        foreach ($order->getAllItems() as $item) {
                            $Allitems[$item->getId()] = $item->getQtyOrdered();
                        }

                        $invoice = $order->prepareInvoice();
                        //  $invoice->register()->pay();

                        Mage::getModel('core/resource_transaction')->addObject($invoice)->addObject($invoice->getOrder())->save();
                    }
                } catch (exception $e) {
                }
                Mage::getSingleton('core/session')->setPluggToNotSave();
            }
        }

        $orderHistory = Mage::getModel('sales/order_status_history')->getCollection()
            ->addFieldToFilter('parent_id', $order->getId());

        $orderHistory = $orderHistory->getData();


        $statusHistory = array();
        $statusComment = array();

        if (is_array($orderHistory)) {
            foreach ($orderHistory as $history) {
                $statusHistory[] = $history['status'];
                $statusComment[] = $history['comment'];
            }
        }


        if(in_array('Order has grouped products',$statusComment) || in_array('Order has bundle products',$statusComment)){
            $grouped = true;
        } else {
            $grouped = false;
        }



        try {

            if (!in_array($status, $statusHistory)) {

                if (isset($state)) {
                    $order->setState($state);
                }



                if ($order->getStatus() != $status){




                    // repõe stock de produtos agrupados ou configuraveis no caso de cancelamento de pedidos
                    if(!$new && $status == Mage::getStoreConfig('pluggto/orderstatus/canceled') && $grouped
                    ){

                            $items = $order->getAllVisibleItems();


                            foreach ($items as $item){

                                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                                $oldq = Mage::getModel('pluggto/product')->getProducQtd($product);
                                $stock = $oldq['qty'];

                                $qtd = (int) $item->getQtyOrdered();


                                $finalQuantity = $stock + $qtd;
                                $array_product = array('quantity'=>$finalQuantity);



                                Mage::getModel('pluggto/product')->setProductStock($product, $array_product);


                            }

                    }

                    $order->addStatusToHistory($status,'PluggTo has updated this status', false);


                }
            }


        } catch (exception $e) {

        }


        Mage::getSingleton('core/session')->setPluggToNotSave(1);
        $order->save();

        Mage::dispatchEvent('sales_order_place_after', array('order' => $order));
        Mage::getSingleton('core/session')->setPluggToNotSave();


    }

    public function getStoreByCode($code){


        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $result = $readConnection->fetchAll("select * from core_config_data where path = 'pluggto/tables_price_customization/table_price' and value = '".$code."' and scope = 'stores' ");

        if(isset($result[0]['scope_id'])){
            return $result[0]['scope_id'];
        } else {
            return null;
        }

    }

    public function savePluggToid($OrderFromPluggto)
    {

        $order = Mage::getModel('sales/order')->load($OrderFromPluggto['external'], 'increment_id');
        $order->setPluggId($OrderFromPluggto['id']);
        $order->save();

    }

    function sanitizeString($string) {

        // matriz de entrada
        $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç','(',')',',',';','|','!','"','#','$','%','=','~','^','>','<','ª','º' );

        // matriz de saída
        $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_' );

        // devolver a string
        return str_replace($what, $by, $string);
    }

    // send to pluggtoTo
    public function update($order, $new = false, $status = false)
    {

        if(!is_object($order)){
            return;
        }

        if ($new) {
            $store = Mage::app()->getStore();
            $name = $store->getName();
            $toPlugg['channel'] = $name;
            $toPlugg['original_id'] = $order->getIncrementId();
        }

        $toPlugg['external'] = $order->getIncrementId();

        switch ($order->getStatus()) {

            case Mage::getStoreConfig('pluggto/orderstatus/partial_payment'):
                $toPlugg['status'] = 'partial_payment';
                break;
            case Mage::getStoreConfig('pluggto/orderstatus/pending'):
                $toPlugg['status'] = 'pending';
                break;
            case Mage::getStoreConfig('pluggto/orderstatus/approved'):
                $toPlugg['status'] = 'approved';
                break;
            case Mage::getStoreConfig('pluggto/orderstatus/invoiced'):
                $toPlugg['status'] = 'invoiced';
                break;
            case Mage::getStoreConfig('pluggto/orderstatus/shipped'):
                $toPlugg['status'] = 'shipping_informed';
                break;
            case Mage::getStoreConfig('pluggto/orderstatus/delivered'):
                $toPlugg['status'] = 'delivered';
                break;
            case Mage::getStoreConfig('pluggto/orderstatus/canceled'):
                $toPlugg['status'] = 'canceled';
                break;
            case Mage::getStoreConfig('pluggto/orderstatus/under_review'):
                $toPlugg['status'] = 'under_review';
                break;
            default:
        }

        $toPlugg['receiver_name'] = $order->getCustomerFirstname();
        $toPlugg['receiver_lastname'] = $order->getCustomerLastname();

        // shipping address

        $shiping = $order->getShippingAddress();

        $DelStree = $shiping->getStreet();


        $toPlugg['receiver_address'] = $DelStree[0];

        if (isset($DelStree[1])) $toPlugg['receiver_address_number'] = $DelStree[1];
        if (isset($DelStree[2])) $toPlugg['receiver_additional_info'] = $DelStree[2];
        if (isset($DelStree[3])) $toPlugg['receiver_neighborhood'] = $DelStree[3];


        $toPlugg['receiver_city'] = $shiping->getCity();
        $toPlugg['receiver_state'] = $shiping->getRegion();
        $toPlugg['receiver_country'] = $shiping->getCountryId();
        $toPlugg['receiver_zipcode'] = $shiping->getPostcode();
        $toPlugg['receiver_phone'] = $shiping->getTelephone();
        $toPlugg['receiver_phone_area'] = '';
        $toPlugg['receiver_email'] = $shiping->getEmail();

        $billing = $order->getBillingAddress();

        $customer = Mage::getModel('customer/customer');
        $customerid = $billing->getCustomerId();

        if (!empty($customerid)) {
            $customer->load($customerid);
            $toPlugg['payer_name'] = $customer->getName();
            $toPlugg['payer_lastname'] = $customer->getLastname();
            $toPlugg['payer_email'] = $customer->getEmail();
        } else {
            $toPlugg['payer_name'] = $order->getCustomerFirstname();
            $toPlugg['payer_lastname'] = $order->getCustomerLastname();
            $toPlugg['payer_email'] = $order->getCustomerEmail();
        }

        $Billstreet = $billing->getStreet();
        $toPlugg['payer_address'] = $Billstreet[0];
        if (isset($Billstreet[1])) $toPlugg['payer_address_number'] = $Billstreet[1];
        if (isset($Billstreet[2])) $toPlugg['payer_address_complement'] = $Billstreet[2];
        if (isset($Billstreet[3])) $toPlugg['payer_neighborhood'] = $Billstreet[3];
        $toPlugg['payer_city'] = $billing->getCity();
        $toPlugg['payer_state'] = $billing->getRegion();
        $toPlugg['payer_country'] = $billing->getCountryId();
        $toPlugg['payer_zipcode'] = $billing->getPostcode();
        $toPlugg['payer_phone'] = $billing->getTelephone();
        $toPlugg['payer_phone_area'] = '';


        $customFieldToStoreCFPorCNPJ = Mage::getStoreConfig('pluggto/configs/custom_document_field');


        if(!empty($customFieldToStoreCFPorCNPJ)){

            $orderData = $order->getData();
            if(isset($orderData[$customFieldToStoreCFPorCNPJ]) && !empty($orderData[$customFieldToStoreCFPorCNPJ])){
                $toPlugg['payer_cpf'] = $orderData[$customFieldToStoreCFPorCNPJ];
            } else {
                $toPlugg['payer_cpf'] =   $order->getVatId();
            }

        }

        $toPlugg['total'] = $order->getGrandTotal();
        $toPlugg['shipping'] = $order->getShippingAmount();
        $toPlugg['subtotal'] = $order->getGrandTotal() - $order->getShippingAmount();

        $payment = $order->getPayment();
        $method = $payment->getMethood();
        $addicional = $payment->getAdditionalData();

        if (($method == 'pluggto' || empty($method)) && !empty($addicional)) {
            $method = $addicional;
        }

        $toPlugg['payment_method'] = $method;

        $shipmentCollection = Mage::getResourceModel('sales/order_shipment_collection')
            ->setOrderFilter($order)
            ->load();

        $toshipmentArray = array();

        foreach ($shipmentCollection as $shipment) {

            foreach ($shipment->getAllTracks() as $trackns) {
                if (!is_null($trackns->getDescription())) {
                    $shipping['shipping_method'] = $trackns->getDescription();
                } else {
                    $shipping['shipping_method'] = $order->getShippingDescription();
                }
                if (!is_null($trackns->getTitle())) $shipping['shipping_company'] = $trackns->getTitle();
                if (!is_null($trackns->getTrackNumber())) $shipping['track_code'] = $trackns->getTrackNumber();
                break;
            }

            if (mageFindClassFile('Thirdlevel_Pluggto_Model_Nfe') != false) {

                $nefClass = Mage::getModel('pluggto/nfe');
                $nef = $nefClass->getNfe($order, $shipment);
                if (isset($nef['nfe_key']) && !empty($nef['nfe_key'])) $shipping['nfe_key'] = $nef['nfe_key'];
                if (isset($nef['nfe_number']) && !empty($nef['nfe_number'])) $shipping['nfe_number'] = $nef['nfe_number'];
                if (isset($nef['nfe_serie']) && !empty($nef['nfe_serie'])) $shipping['nfe_serie'] = $nef['nfe_serie'];
                if (isset($nef['nfe_date']) && !empty($nef['nfe_date'])) $shipping['nfe_date'] = $nef['nfe_date'];
                if (isset($nef['nfe_key']) && !empty($nef['nfe_key'])) $shipping['nfe_key'] = $nef['nfe_key'];
                if (isset($nef['nfe_link']) && !empty($nef['nfe_link'])) $shipping['nfe_link'] = $nef['nfe_link'];
            }
        }

        if (!isset($shipping['nfe_number']) || empty($shipping['nfe_serie'])) {
            {
                $_history = $order->getAllStatusHistory();

                foreach ($_history as $_historyItem) {

                    $_historyItemAllComment = $_historyItem->getData('comment');


                    if (!empty($_historyItemAllComment)) {

                        $_historyItemEachLine = explode('\r\n', $_historyItemAllComment);


                        if (!empty($_historyItemEachLine)) {

                            foreach ($_historyItemEachLine as $line) {

                                $_historyItemEachBR = explode('<br/>', $line);

                                if (!empty($_historyItemEachBR)) {

                                    foreach ($_historyItemEachBR as $_historyItemEachBRComBarra) {

                                        $_historyItemEachBRSEMBARRA = explode('<br>', $_historyItemEachBRComBarra);

                                        if (!empty($_historyItemEachBRSEMBARRA)) {

                                            foreach ($_historyItemEachBRSEMBARRA as $_historyItem) {

                                                $this_historyItem = $this->sanitizeString(strip_tags($_historyItem));

                                                if (preg_match("/Nota fiscal/",trim($this_historyItem))) {

                                                    $notfiscal = explode(':', $this_historyItem);

                                                    if (isset($notfiscal[1])) {
                                                        $nfe_number = strip_tags(trim($notfiscal[1]));
                                                    }

                                                }

                                                if (preg_match("/Nr NF-e/",trim($this_historyItem))) {

                                                    $notfiscal = explode(':', $this_historyItem);

                                                    if (isset($notfiscal[1])) {
                                                        $nfe_number = strip_tags(trim($notfiscal[1]));
                                                    }

                                                }


                                                if (preg_match("/Serie/", trim($this_historyItem))) {

                                                    $serie = explode(':', $this_historyItem);

                                                    if (isset($serie[1])) {
                                                        $nfe_serie = strip_tags(trim($serie[1]));
                                                    }

                                                }



                                                if (preg_match("/Chave de Acesso/", trim($this_historyItem))) {
                                                    $chave = explode(':', trim($this_historyItem));

                                                    if (isset($chave[1])) {
                                                        $nfe_key = strip_tags(trim($chave[1]));

                                                    }

                                                }

                                                if (preg_match("/Link da DANFE/", trim($this_historyItem))) {

                                                    $link = explode(':', trim($this_historyItem));

                                                    if (isset($link[1]) && isset($link[2])) {
                                                        $nfe_link = $link[1] .':'. strip_tags(trim($link[2]));
                                                    }

                                                    if(isset($nfe_link) && isset($link[3])){
                                                        $nfe_link = $nfe_link .':'. $link[3];
                                                    }

                                                }

                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }

            }
            if(!isset($shipping)) $shipping = array();
            if(isset($nfe_number)) $shipping['nfe_number'] = $nfe_number;
            if(isset($nfe_serie)) $shipping['nfe_serie'] = $nfe_serie;
            if(isset($nfe_key)) $shipping['nfe_key'] = $nfe_key;
            if(isset($nfe_link)) $shipping['nfe_link'] = $nfe_link;

        }


        if (isset($shipment) && is_object($shipment) && !is_null($shipment->getIncrementId())) $shipping['external'] = $shipment->getIncrementId();
        if (isset($shipment) && is_object($shipment) && !is_null($shipment->getCreatedAt())) $shipping['date_shipped'] = $shipment->getCreatedAt();

        if(!empty($shipping)){
            $toshipmentArray[] = $shipping;
        }

        if(!empty($toshipmentArray)){
            $toPlugg['shipments'] = $toshipmentArray;
        }



        $pluggtoId = $order->getPluggId();

        if (!empty($pluggtoId) && isset($toPlugg['shipments']) && !empty($toPlugg['shipments'])) {

            $old = Mage::getModel('pluggto/api')->get('orders/' . $pluggtoId, null, null, true);

            if (isset($old['Body']['Order']['shipments'][0]['id'])) {
                $toPlugg['shipments'][0]['id'] = $old['Body']['Order']['shipments'][0]['id'];
            }

        }

        $toPlugg['purchased'] = $order->getCreatedAt();
        if ($new) $toPlugg['created'] = $order->getCreatedAt();
        $toPlugg['modified'] = $order->getUpdatedAt();

        $items = $order->getAllVisibleItems();

        $i = 0;

        if ($new):
            foreach ($items as $item):


                $product = Mage::getModel('catalog/product')->load($item->getProductId());

                $toPlugg['items'][$i]['name'] = $item->getName();
                $toPlugg['items'][$i]['price'] = $item->getPrice();
                $toPlugg['items'][$i]['quantity'] = $item->getQtyOrdered();
                $toPlugg['items'][$i]['total'] = $item->getQtyOrdered() * $item->getPrice();
                $toPlugg['items'][$i]['sku'] = $product->getSku();
                $toPlugg['items'][$i]['external'] = $product->getId();


                if ($product->getStockItem()->getProductTypeId() == 'configurable') {

                    $options = $item->getProductOptions();

                    try {
                        $frompluggto = Mage::getSingleton('pluggto/api')->get('products/' . $product->getPluggtoId(), null, null, true);
                    } catch (exception $e) {
                        Mage::helper('pluggto')->WriteLogForModule('Error', 'Item não encontrado no plugg.to');
                    }


                    $vari = array();
                    if (isset($frompluggto['Product']['variations']) && is_array($frompluggto['Product']['variations'])) {


                        foreach ($frompluggto['Product']['variations'] as $varis) {
                            $vari[$varis['id']] = $varis;
                        }

                    }

                    $subproduct = Mage::getSingleton('catalog/product')->load($product->getIdBySku($options['simple_sku']));

                    $toPlugg['items'][$i]['variation']['id'] = $subproduct->getPluggtoId();
                    $toPlugg['items'][$i]['variation']['sku'] = $subproduct->getSku();
                    $toPlugg['items'][$i]['variation']['name'] = $subproduct->getName();


                    if (isset($vari[$subproduct->getPluggtoId()])) {

                        if (isset($vari[$subproduct->getPluggtoId()]['attributes']) && is_array($vari[$subproduct->getPluggtoId()]['attributes'])) {
                            $j = 0;
                            foreach ($vari[$subproduct->getPluggtoId()]['attributes'] as $attribute) {
                                if (isset($attribute['code'])) $toPlugg['items'][$i]['variation']['attributes'][$j]['code'] = $attribute['code'];
                                if (isset($attribute['label'])) $toPlugg['items'][$i]['variation']['attributes'][$j]['label'] = $attribute['label'];
                                if (isset($attribute['value']['code'])) $toPlugg['items'][$i]['variation']['attributes'][$j]['value']['code'] = $attribute['value']['code'];
                                if (isset($attribute['value']['label'])) $toPlugg['items'][$i]['variation']['attributes'][$j]['value']['label'] = $attribute['value']['label'];
                                $j++;
                            }

                        }
                    }

                }

                $i++;

            endforeach;
        endif;// if new

        return $toPlugg;

    }

    public function forceSyncOrders()
    {

        $api = Mage::getSingleton('pluggto/api');
        $post['order'] = 'desc';
        $post['limit'] = 100;
        $orders = $api->get('orders', $post, 'field', true);


        foreach ($orders['Body']['result'] as $order) {
            $this->create($order['Order']);
        }
    }



    public function forceUpdateOrders()
    {

        $modelOrder = Mage::getModel('sales/order');
        $modelOrderCollection = $modelOrder->getCollection()->addAttributeToFilter('plugg_id', array('neq' =>''))->setOrder('entity_id', 'DESC')->setPageSize(300);
        $queue =  Mage::getModel('pluggto/export');


        foreach($modelOrderCollection as $thisOrder){
            $queue->exportOrderToQueue($thisOrder->getEntityId());
        }

        return true;

    }

}
